from django.db import models

# Create your models here.
class Dia(models.Model):
    nombre = models.CharField(max_length=20)
    numero_dia = models.IntegerField(unique=True)
