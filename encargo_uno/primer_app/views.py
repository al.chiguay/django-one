from django.shortcuts import render

# Create your views here.
def index(request):
    context={"hola_mundo":"Este es el hola mundo que viene del context"}
    return render(request, 'index.html', context)

def siguiente(request):
    dias=["lunes","martes","miercoles","jueves","viernes"]
    context={"dias_de_la_semana":dias}
    return render(request, 'siguiente.html',context)