from django.urls import path

from . import views

app_name = 'primer_app'

urlpatterns = [
    path('', views.index, name="index"),
    path('pag_dos/', views.siguiente, name="siguiente"),
]
