# django-one
1. cree el proyecto
2. cree una app "primer_app"
3. agregue la app en settings.py
4. cree el archivo urls.py en la app
5. en views.py de la aplicacion cree dos vistas y las inclui en el archivo urls.py
6. en el urls.py general ingresé include
7. cree los template para las 2 vistas y les inclui el contexto
8. cree un modelo
9. registre el modelo en admin.py
10. cree el superusuario para entrar desde localhost/admin

